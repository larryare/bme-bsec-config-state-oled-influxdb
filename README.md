# BME688 Config state with oled display and influxdb

## How to use

Create config.h with following contents:
```cpp
#define WIFI_SSID "SSID"
#define WIFI_PASSWORD "PASSWORD"
#define INFLUXDB_URL "http://influxdb.yourmomlol.com"
#define INFLUXDB_TOKEN "MYSECRET"
```